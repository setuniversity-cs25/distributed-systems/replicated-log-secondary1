# syntax=docker/dockerfile:1

FROM golang:1.21-alpine as builder

WORKDIR /app

COPY go.mod go.sum ./
RUN go mod download

COPY . .

WORKDIR /app/cmd/replicated-log
RUN CGO_ENABLED=0 GOOS=linux go build -o /app/replicated-log

# Start a new stage
FROM alpine:latest

WORKDIR /app

# Copy the binary from the builder stage
COPY --from=builder /app/replicated-log .

EXPOSE 8081

CMD ["./replicated-log"]

